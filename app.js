var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Define a variavel do arquivo de rotas
// var indexRouter = require('./routes/index');
var cursosRouter = require('./routes/cursos');

var app = express();

// Configuração do parse de template
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Define as rotas da api
// app.use('/', indexRouter);
app.use('/api/cursos', cursosRouter);

// Detecta o 404 e envia pro handler 
app.use(function(req, res, next) {
  next(createError(404));
});

// Handler de erros
app.use(function(err, req, res, next) {
  // seta variaveis locais, mostrando debug apenas em desenvolvimento
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // Carrega a pagina de erro
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
