# Instruções de utilização
Requisitos :

- Ter instalado o node.js juntamente com o NPM


Utilização :

- Instalar as dependencias de dev, estando na raiz do projeto :

```
npm install
```

- Rodar a api utilizando :

```
npm start
```

# Informações adicionais
Para rodar os testes unitarios, estando na raiz do projeto :

```
npm test
```

Os testes ficam dentro de test/spec/ e devem ter o nome como "xxxxx.spec.js"

Site de documentação do [Express](http://expressjs.com/)

Site de documentação do [Mocha](https://mochajs.org/)

Site de documentação dos asserts [Chai](http://www.chaijs.com/api/assert/)

Site de documentação dos fakes [Sinon](https://sinonjs.org/)


----

Copyright (C) 2018 Armindo Junior