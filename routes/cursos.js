const express = require('express');
const router = express.Router();
const Joi = require('joi');

// Funções úteis
function procuraCurso(identificador) {
  return cursos.find(c => c.id === identificador);
};

function validaCurso(curso){
  const schema = {
      nome: Joi.string().min(3).required()
  };

  return Joi.validate(curso, schema);
};

const cursos = require('../data/data.json');

router.get('/', (req, res) => {
  res.send(cursos);
});

router.post('/', (req, res) => {
  const { error } = validaCurso(req.body);

  // Se não for valido retornar 400
  if(error){
      // Bad request
      res.status(400).send(result.error.details[0].message);
      return;
  }

  const curso = {
      id: cursos.length + 1,
      nome: req.body.nome
  };

  cursos.push(curso);
  res.send(curso);
});

router.get('/:id', (req, res) => {
  // res.send(req.query) == /api/teste/1?sortBy=nome
  const curso = procuraCurso(parseInt(req.params.id));

  if (!curso) {
      res.status(404).send('Curso não encontrado!');
      return;
  } else {
      res.send(curso);
  }

});

router.put('/:id', (req, res) => {
  const curso = procuraCurso(parseInt(req.params.id));

  if (!curso) {
      res.status(404).send('Curso não encontrado!');
      return;
  }

  const { error } = validaCurso(req.body);

  if(error){
      // Bad request
      res.status(400).send(error.details[0].message);
      return;
  }


  curso.nome = req.body.nome;
  res.send(curso);
});

router.delete('/:id', (req, res) => {
  const curso = procuraCurso(parseInt(req.params.id));

  if (!curso) {
      res.status(404).send('Curso não encontrado!');
      return;
  }

  const index = cursos.indexOf(curso);
  cursos.splice(index, 1);

  res.send(`Curso ${curso.nome} excluido`);
});

module.exports = router;
