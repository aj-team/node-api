const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
let server = require('../../app');

chai.use(chaiHttp);

describe('Cursos', () => {
    it('GET cursos', () => {
        chai.request(server)
            .get('/api/cursos')
            .end((err, res) => {
                let len = JSON.parse(res.text).length;
                assert.equal(len, 3);
            });
    });

    it('GET curso #1', () => {
        chai.request(server)
            .get('/api/cursos/1')
            .end((err, res) => {
                let parse = JSON.parse(res.text);
                assert.equal(parse.nome, 'teste 1');
            });
    });

    it('POST curso', () => {
        let exemplo = {
            nome: "Teste automatico"
        }

        chai.request(server)
            .post('/api/cursos')
            .send(exemplo)
            .end((err, res) => {
                let parse = JSON.parse(res.text);
                assert.equal(parse.nome, 'Teste automatico');
            });
    });

    it('PUT curso #1', () => {
        let exemplo = {
            nome: "Teste novo"
        }

        chai.request(server)
            .put('/api/cursos/1')
            .send(exemplo)
            .end((err, res) => {
                let parse = JSON.parse(res.text);
                assert.equal(parse.nome, 'Teste novo');
            });
    });

    it('DELETE curso #1', () => {
        chai.request(server)
            .delete('/api/cursos/1')
            .end((err, res) => {
                let parse = res.text;
                assert.equal(res.text, 'Curso Teste novo excluido');
            });
    });
});